# #using npm
# from node:lts as build 
# workdir /app 
# copy package*.json ./ 
# run npm install 
# copy . . 
# run npm run build 


# #  create production ready for nextjs container 
# from nginx:alpine
# copy --from=build /app/.next/server/pages /usr/share/nginx/html
# expose 80
# cmd ["nginx", "-g", "daemon off; "]


#section one 
# from node:18.12.1 as build 
# workdir /app
# copy ./ ./ 
# run npm install --force 
# run npm run build 


# # section two 
# FROM nginx:1.23.2
# COPY default.conf /etc/nginx/conf.d/default.conf 
# COPY --from=build /app/build /usr/share/nginx/html 

# EXPOSE 80 
# CMD ["nginx", "-g", "daemon off;"]



# Build stage
FROM node:lts as build 
WORKDIR /app 
COPY package*.json ./ 
RUN npm install 
COPY . . 
RUN npm run build 

# Production stage
FROM node:lts
WORKDIR /app
COPY --from=build /app ./
RUN npm install -g serve
EXPOSE 3000
CMD ["npm", "start"]




